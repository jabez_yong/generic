<?php

namespace Werebits\ContactUs\Controllers;

use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    public function contact(){
        return view('contactus::demo');
    }
}
