<?php

namespace Werebits\ContactUs;

use Illuminate\Support\ServiceProvider;

class ContactServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        require __DIR__.'/routes/routes.php';
        $this->loadViewsFrom(__DIR__.'/views/', 'contactus');

        if ($this->app->runningInConsole()) {
            $this->commands([
                \Werebits\ContactUs\Commands\InstallCommand::class,
            ]);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
